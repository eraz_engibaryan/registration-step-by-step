<?php
require __DIR__ . '/../db_connection.php';

class Customers
{

    protected $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        // TODO: Implement __destruct() method.
        $this->db = null;
    }

    public function create($data)
    {
        $query = 'INSERT INTO customers (';

        $i = 1;
        foreach ($data as $name => $value) {
            if ($i < count($data))
                $query .= $name . ', ';
            else
                $query .= $name;

            $i++;
        }

        $query .= ') VALUES (';

        $i = 1;
        foreach ($data as $name => $value) {
            if ($i < count($data))
                $query .= '?, ';
            else
                $query .= '?';

            $i++;
        }

        $query .= ')';

        $query = $this->db->prepare($query);

        $values = [];

        foreach ($data as $key => $val) {

            array_push($values, $val);
        }

        $query->execute($values);

        $lastID = DB()->lastInsertId();

        return $lastID;
    }

    public function update($id, $data)
    {

        $query = 'UPDATE customers SET ';

        $i = 1;
        foreach ($data as $name => $item) {
            if ($i < count($data)) {
                $query .= $name . ' = ?, ';
            } else {
                $query .= $name . ' = ?';
            }
            $i++;
        }
        $query .= ' WHERE id = ' . $id;
        $query = $this->db->prepare($query);

        $items = [];

        foreach ($data as $name => $item) {

            array_push($items, $item);
        }
        $query->execute($items);
    }


}




