<?php
/**
 * Request from registration function
 */
require '../model/CustomersModel.php';
$customer = new Customers();

switch ($_POST['step']) {

    case 1:
        // create

        $lastID = $customer->create($_POST['fields']);

        echo $lastID;
        break;
    case 2:
        // update
        $lastID = $_POST['id'];
        $customer->update($lastID, $_POST['fields']);
        break;
    case 3:
        // update
        $fields = $_POST['fields'];
        $lastID = $_POST['id'];
        $iban = $fields['iban'];
        $owner = $fields['owner'];

        $customer->update($lastID, $fields);

        if ($curl = curl_init()) {

            $data = [
                'customerId' => $lastID,
                'iban' => $iban,
                'owner' => $owner
            ];
            $data = json_encode($data);

            curl_setopt($curl, CURLOPT_URL, 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $out = curl_exec($curl);
            $out = json_decode($out, true);
            $responce = $out['paymentDataId'];
            $customer->update($lastID, $out);

            curl_close($curl);
            echo $responce;
        }

        break;
}

