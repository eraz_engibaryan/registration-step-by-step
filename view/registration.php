<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Registration</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

    <link rel="stylesheet" href="css/style.css">

</head>

<body>

<!-- multistep form -->
<form id="msform" action="../controller/action_page.php">
    <!-- progressbar -->
    <ul id="progressbar">
        <li class="active">Personal Info</li>
        <li>Address Info</li>
        <li>Payment Info</li>
    </ul>
    <!-- fieldsets -->
    <fieldset id="step1">
        <h2 class="fs-title">Personal Info</h2>
        <h3 class="fs-subtitle">Step 1</h3>

        <input type="text" class="firstname" placeholder="First name" name="firstname" required>
        <input type="text" class="lastname" placeholder="Last name" name="lastname" required>
        <input type="tel" class="phone" placeholder="Telephone" name="phone">
        <button data-step="1" class="next action-button step1">Next</button>

    </fieldset>
    <fieldset id="step2">
        <h2 class="fs-title">Address Info</h2>
        <h3 class="fs-subtitle">Step 2</h3>

        <input type="text" placeholder="Street" name="street" required>
        <input type="text" placeholder="House number" name="house_number" required>
        <input type="text" placeholder="Zip code" name="zip_code" required>
        <input type="text" placeholder="City" name="city" required>
        <button data-step="2" class="next action-button step2">Next</button>

    </fieldset>
    <fieldset id="step3">
        <h2 class="fs-title">Payment Info</h2>
        <h3 class="fs-subtitle">We will never sell it</h3>

        <input type="text" placeholder="Account owner" name="owner" required>
        <input type="text" placeholder="IBAN" name="iban" required>
        <button data-step="3" class="next action-button step3">Register</button>


    </fieldset>
</form>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>


<script src="js/custom.js"></script>


</body>

</html>
