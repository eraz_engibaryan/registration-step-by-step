$(document).ready(function () {


    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    var phone, firstname;
    var step, owner;
    var toContinue = localStorage.getItem('step');

    if (toContinue) {
        var _step = parseInt(localStorage.getItem('step'));
        var user_id = localStorage.getItem('user_id');

        if (confirm("Continue registration?")) {


            $('fieldset').hide();

            var i = 0;
            while (i < _step) {
                $("#progressbar li").eq(i + 1).addClass("active");
                i++;
            }

            current_fs = $('#step' + _step);
            next_fs = $('#step' + (_step + 1));

            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({
                        'transform': 'scale(' + scale + ')',
                        'position': 'absolute'
                    });
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        } else {
            localStorage.removeItem("user_id");
            localStorage.removeItem("step");
        }

    }

    $(".next").click(function (e) {
        e.preventDefault();

        var fields = {};
        var inputs = $(this).closest('fieldset').find('input');


        $.each(inputs, function (index, input) {

            fields[$(input).attr('name')] = $(input).val();


        });


        step = $(this).data('step');

        var url = '../controller/action_page.php';

        $.ajax({
            url: url,
            type: 'post',
            data: {
                fields: fields,
                step: step,
                // iban: iban,
                // owner: owner
                id: user_id
            },
            success: function (data) {

                switch (step) {
                    case 1:
                        localStorage.setItem("user_id", data);
                        localStorage.setItem("step", step);
                        user_id = data;
                        break;
                    case 2:
                        localStorage.setItem("step", step);
                        break;
                    case 3:
                        // localStorage.setItem("step", step);
                        localStorage.removeItem("user_id");
                        localStorage.removeItem("step");
                        window.location.href = '../view/responce.php?paymentDataId=' + data;
                        break;
                }

            }
        });


        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'position': 'absolute'
                });
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
    });

    $(".submit").click(function () {
        return false;
    });
})
;